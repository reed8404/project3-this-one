package app;

/*util*/
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/*utilities*/
import utilities.*;
/*camparators*/
import comparators.*;


/**
 * 
 * @author Reed
 */
public class ProductSorter {
	/**
	 * matches user input with sorter option(app functionality)
	 * @param productList, list of product inventory 
	 */
	 public static void selectSorterMenuOp(List<Product> productList) { 
		 displaySorterMenuOptions();
		 int useerInput;
	     Scanner scanner = new Scanner(System.in);
			do {
	            useerInput = scanner.nextInt();
	            
	            switch (useerInput) {
	                case 1:
	                	System.out.println("Sort By Name");
	                	Collections.sort(productList, new NameComparator());
	                	Displayer.displayProduct(productList);
	                    break;
	                case 2:
	                	System.out.println("Sort By Category");
	                	Collections.sort(productList, new CategoryComparator());
	                	Displayer.displayProduct(productList);
	                    break;
	                case 3:
	                	System.out.println("Sort By Price");
	                	Collections.sort(productList, new PriceComparator());
	                	Displayer.displayProduct(productList);
	                    break;
	                case 4:
	                	System.out.println("Sort Again");
	                	selectSorterMenuOp(WareHouse.productList);
	                    break;
	                case 5:
	                	System.out.println("Main Menu");
	                	SurvivalStore.displayMainMenuWelcomeMessage();
	                	SurvivalStore.selectMenuOp();
	                    break;
	                case 6:
	                	System.out.println("Exiting Survival Store");
	                	System.out.println("Thank you for using our store app! Have a gread day :D");
	                    System.exit(0);
	                    break;
	                default:
	                    System.out.println("The value you entered is not within the required range for this program (1..6), please re-enter\n");
	                    break;
	            } 
	        }
	        while (useerInput != 6);
			scanner.close();
	 } // end of selectSorterMenuOp
	 
/********************sorter message********************/
	 
	 /**
	 * displays sorter welcome message
	 */
	 public static void displaySorterWelcomeMessage() {
		 System.out.println("Welcome to the Product Sorter, please enter a value between (1..6) \n\n");
	 }
	    
	  /**
	  * displays sorter menu options
	  */
	 public static void displaySorterMenuOptions() {
		 System.out.println("The menu is setup in the following way: \n\n"
	                         + "To sort products by name, enter: 1\n"
	                         + "To sort products by category, enter: 2\n"
	                         + "To sort products by price, enter: 3\n"
	                         + "To reenter Product Sorter, enter: 4\n"
	                         + "To return to the Main Menu, enter: 5\n"
	                         + "To exit Survival Store, enter: 6\n" );
	 }
}


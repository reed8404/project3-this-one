package app;

/*util*/
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/*html*/
import javax.swing.text.html.HTMLDocument.Iterator;

/**
 * 
 * @author Reed
 */
public class ShoppingCart {
	/*
	 * create users shopping cart to hold products
	 * total number of products in users shopping cart
	 */
	private List<Product> shoppingCartList = new ArrayList<>();
	private int productsInCart = 0;
	
	/**
	 * matches user input with shopping cart option(app functionality)
	 * @param wareHouse
	 */
	public void selectShoppingCartMenuOp(WareHouse wareHouse) {
		int choice = 0;
		Scanner scanner = new Scanner(System.in);
	    displaysShoppingCartMenuOptions();
	    
	    do{
	       choice = scanner.nextInt();
	       
	       switch (choice) {
	       		case 1:
	       			System.out.println("View Items In Your Cart");
	       			// SEE displayProducts in Displayer
	       			// printItemsInCart();
	                break;
	            case 2:
	            	System.out.println("Add Item/s To Your Cart");
	                System.out.println("So far there are + " + productsInCart + "items in your cart...\n");
	                String temp = (performAdd(wareHouse)) ? "Add was a success!" : "Something went wrong, please re-select add to try again :/\n";
	                break;
	            case 3:
	            	System.out.println("Remove Item/s From Your Cart");
	                break;
	            case 4:
	            	System.out.println("View Total Cost Of Your Cart");
	                break;
	            case 5:
	            	System.out.println("Return To Main Menu");
	                System.out.println("Thank you for using the store app! Have a gread day :D");
	                break;
	            case 6:
	            	System.out.println("Exiting Survival Store");
                    System.out.println("Thank you for using our store app! Have a gread day :D");
                    System.exit(0);
                    break;
	            default:
	                System.out.println("The value you entered is not within the required range for this program (1..6), please re-enter\n");
	                break;
	            }   
	    }
	    while (choice != 6);
	    scanner.close();
	}
	
	/**
	 * displays shopping cart menu options
	 */
	private void displaysShoppingCartMenuOptions() {
	        System.out.println("This is the cart menu: \n"
	                         + "To view all Items in your cart currently enter 1\n"
	                         + "To add item/s to your cart enter 2\n"
	                         + "To remove item/s from your cart enter 3\n"
	                         + "To view total cost for your cart enter 4\n"
	                         + "To exit cart menu and return to main menu enter 5"
	                         + "To exit application enter: 6\n");
	}
// SEE displayProducts in Displayer
//	private void printItemsInCart() {
//		if (shoppingCartList.isEmpty()) {
//			System.out.println("Currently you have no items in the cart.\n");
//	    }
//	    else {
//	    	Iterator iterator = shoppingCartList.iterator();
//	        
//	            while (iterator.hasNext()){
//	                
//	                System.out.println(iterator.next());
//	            
//	            }
//	        }
//	    }
	
	/**
	 * adds product & increments number of products in users shopping cart
	 * @param product, instance of product to be added to shoppingCartList
	 */
	public void addToCart(Product product) {
		shoppingCartList.add(product);
	    productsInCart++;
	}
	
	/**
	 * removes product & decrements number of products in users shopping cart
	 * @param product, product to be removed from users shopping cart
	 */
	public void removeFromCart(Product product) {
		shoppingCartList.remove(product);
	    productsInCart--;
	}
	 
	/**
	 * @return shoppingCartList, list of products the user has selected
	 */
	public List<Product> getCartList() {
		return shoppingCartList;
	}
	
	/**
	 * Allows user to add product from to shopping cart
	 * @param warehouse, list of products in inventory
	 * @return successReturnValue, true if product added, false if not added
	 */
	private boolean performAdd(WareHouse wareHouse) {
	    boolean successReturnValue = false;
	    
	    /*
	     * prompts & receives input
	     */
	    Scanner scanner = new Scanner(System.in);
	    System.out.println("Please provide the name of the item you would like to add to the cart");
	    String userInput = scanner.nextLine();
	 
	    Iterator iterator = (Iterator) wareHouse.productList.iterator();
	    Product product = new Product(); // empty product, default constructor
	    int i = 0; // index of product to get from the list of products/inventory
	    
	    /*
	     * iterate through shopping cart list,
	     * if user input matches name of product, 
	     * get product from list of products/inventory,
	     * add product to shopping cart
	     * successfully returned
	     */
	    while (iterator.hasNext()) {
	        if (userInput.equals(wareHouse.productList.get(i).getName())) {
	        	product = wareHouse.productList.get(i); // grab the product object from inventory list and assign empty product object
	        	wareHouse.productList.remove(i); // remove this object from the inventory list
	        	addToCart(product); // call to in class method for cart
	        	successReturnValue = true; // change boolean return value
	        }
	        i++; // increment index 
	    }
	    scanner.close();
	    return successReturnValue;
	} // end of performAdd
	
	/**
	 * Allows user to remove product from shopping cart
	 * @param warehouse, list of products in inventory
	 * @return successReturnValue, true is product returned, false if not returned
	 */
	private boolean performSubtract(WareHouse warehouse) {
	    boolean successReturnValue = false;
	    
	    /*
	     * prompts & receives input
	     */
	    Scanner scanner = new Scanner(System.in);
	    System.out.println("Please provide the name of the item you would like to remove from the cart");
	    String userInput = scanner.nextLine();
	    
	    Iterator iterator = (Iterator) shoppingCartList.iterator();
	    Product product = new Product(); // empty product, default constructor
	    int i = 0; // index of product to get from the shopping cart
	    
	    /*
	     * iterate through shopping cart list,
	     * if user input matches name of product, 
	     * get product from shopping cart,
	     * add product back to warehouse
	     * remove product from cart
	     * successfully returned
	     */
	    while (iterator.hasNext()) { 
	        if (userInput.equalsIgnoreCase(shoppingCartList.get(i).getName())) {
	        	product = shoppingCartList.get(i);
	            warehouse.productList.add(product);
	            removeFromCart(product);
	            successReturnValue = true;
	        }
	        i++; // increment index 
	    }
	    scanner.close();
	    return successReturnValue;
	} // end of performSubtract
	
	/**
	 * iterates through products in shopping cart, 
	 * gets price of product as String, removes '$',
	 * converts String to double,
	 * @return totalPrice, collective price of products in shopping cart
	 * @throws NullPointerException
	 */
	private Double getTotal() throws NullPointerException {
		Iterator iterator = (Iterator) shoppingCartList.iterator();
	    Double totalPrice = 0.0;
	    int i = 0;
	    
	    while (iterator.hasNext()) {
	            String temp = shoppingCartList.get(i).getPrice().replaceAll("$", "");
	            totalPrice = Double.parseDouble(temp);  
	    }   
	    return totalPrice; 
	} 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Reed
 */
public class Product {
    /**********name & category**********/
	private String name = "";
    private String category = "";
    
    /**********line: all the comma separated values associated with one product**********/
    private String line = ";-)";
    private final String CSV_SPLIT_VALUE = ",";
    
    /**********price**********/
    private String price = "";
    private Double priceNum = 0.0;
    
    /**********inventory**********/
    private String inventory = "";
    private int inventoryNum = 0;
    
    /**
     * default constructor 
     */
    public Product(){
      
    }
    
    /**
     * 
     * @param line
     */
    public Product(String line) {
    	this.line = line;
    	splitLine(line);
    }
    
//    /**
//     * 
//     * @param category
//     * @param name
//     * @param price
//     * @param inventory
//     */
//    public Product(Product category, Product name, Product price, Product inventory) {
//        this.name = name;
//        this.category = category;
//        
//        /******price******/
//        this.price = price;
//        setPriceNum(price);
//        
//        /******inventory******/
//        this.inventory = inventory;
//        setInventoryNum(inventory);
//    }
    
    @Override
    public String toString() {
    	return line;
    }
    
    /**
     * 
     * @param line
     */
    private void splitLine(String line) {
		String[] productInfo = line.split(CSV_SPLIT_VALUE);
		
		this.category = productInfo[0];
		this.name = productInfo[1];
		this.price = productInfo[2];
		this.inventory = productInfo[3];
		
	}
    /********************get & set********************/
    
    /**********name***********/
    
	/**
     * 
     * @return
     */
    public String getName(){
        return name;
    }
    
    /**
     * 
     * @param name
     */
    public void setName(String name){
        this.name = name;
    }
    
    /**********category**********/
    
    /**
     * 
     * @return
     */
    public String getCategory(){
        return category;
    }
    
    /**
     * 
     * @param category
     */
    public void setCategory(String category){
        this.category = category;
    }
    
    /**********price**********/
    
    /**
     * 
     * @return
     */
    public String getPrice(){
        return price;
    }
    
    /**
     * 
     * @param price
     */
    public void setPrice(String price){
        this.price = price;
    }
    
    /**
     * 
     * @return
     */
    public double getPriceNum() {
    	return priceNum;
    }
    
//    /**
//     * 
//     * @param price
//     */
//    private void setPriceNum(Product price) {
//    	priceNum = Double.parseDouble(price.replace("$", ""));
//    }
    
    /**********inventory**********/
    
    /**
     * 
     * @return
     */
    public String getInventory(){
        return inventory;
    }
    
    /**
     * 
     * @param inventory
     */
    public void setInventory(String inventory){
        this.inventory = inventory;
    }
    
    /**
     * 
     * @param inventory
     */
    private void setInventoryNum(String inventory) {
    	inventoryNum = Integer.parseInt(inventory);
    }
    
    /**
     * 
     * @return
     */
    public boolean isInventoryInStock() {
    	return (inventoryNum > 0 ? true : false);
    }
    
    /**
     * 
     * @param inventory
     * @return
     */
    public boolean subtractInventoryValue(int inventory) {
    	if(isInventoryInStock()) {
    		inventoryNum = inventoryNum - inventory;
    		return true;
    	}
    	else {
    		return false;
    	}
    }
}

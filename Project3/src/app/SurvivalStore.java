package app;

/*io*/
import java.io.IOException;

/*util*/
import java.util.Scanner;

import utilities.*;

/**
 *
 * @author Reed
 */
public class SurvivalStore {

    /**
     * entry point of application
     * declare-instantiate-initialize wareHouse, pass file location
     * call loadData, read from file, initialize instances of product with files data
     * 
     * declare-instantiate-initialize wallet
     * provide minimum & maximum values to setRandomStartingBalance, 
     * balance is assigned a random double value
     * 
     * print 'logo'
     * print welcome message & main menu options 
     * accept users input & provide requested features
     * 
     * @param args the command line arguments
     */
    public static void main(Product[] args) throws IOException {
        
        WareHouse wareHouse = new WareHouse("resources/survival_store_inventory.csv");
        wareHouse.loadData();
        
        Wallet wallet = new Wallet();
        wallet.setRandomStartingBalance(20.0, 500.0);
        
        Displayer.displaySurvivalStoreLogo();
        displaySurvivalStoreWelcomeMessage();
        displayMainMenuWelcomeMessage();
        selectMenuOp();
    }
    
    /**
     * matches user input with Main Menu option(app functionality)
     */
    public static void selectMenuOp() { 
    	displayMainMenuOptions();
        int choice;
        
        Scanner scanner = new Scanner(System.in);
		do {
            choice = scanner.nextInt();
            
            switch (choice){
                
                case 1:
                	System.out.println("All Products");
                	//TO-DO: Move to displayProducts in Displayer
                    wareHouse.displayProducts();
                    break;
                case 2:
                	System.out.println("Sort Products");
                	ProductSorter.displaySorterWelcomeMessage();
                	ProductSorter.selectSorterMenuOp(wareHouse.productList);
                    break;
                case 3:
                	System.out.println("Your Shopping Cart");
                	doCartStuff(productList, new ShoppingCart());
                    break;
                case 4:
                	System.out.println("Your Wallet");
                	wallet.getBalance(); 
                    break;
                case 5:
                	System.out.println("Exiting Survival Store");
                    System.out.println("Thank you for using our store app! Have a gread day :D");
                    System.exit(0);
                    break;
                default:
                    System.out.println("The value you entered is not within the required range for this program (1..5), please re-enter\n");
                    break;
            } 
        }
        while (choice != 5);
    } //end of selectMenuOp

/********************Survival Store & Main Menu Message********************/
    
    /**
     * displays Survival Store welcome message
     */
    public static void displaySurvivalStoreWelcomeMessage(){
        System.out.println("Hello! This is our text based store application.\n\n");
    }
    
    /**
     * displays Main Menu welcome message
     */
    public static void displayMainMenuWelcomeMessage(){
        System.out.println("Welcome to the Main Menu, please enter a value between (1..5) \n\n");
    }
    
    /**
     * displays Main Menu options
     */
    public static void displayMainMenuOptions() {
    	 System.out.println("The menu is setup in the following way: \n\n"
                         + "To see all products enter: 1\n"
                         + "To go into the sort menu enter: 2\n"
                         + "To view cart enter: 3\n"
                         + "To check wallet enter: 4\n"
                         + "To exit application enter: 5\n" );
    }
    
}

package comparators;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import app.*;

/**
 * 
 * @author Reed
 *
 */
public class NameComparator implements Comparator<Product> {

	@Override
	public int compare(Product o1, Product o2) {
		return o1.getName().compareToIgnoreCase(o2.getName());
	}
//	@Override
//	public int compare(Object productPra, Object productPra2) {
//		Product productLoc = (Product)productPra;
//		Product productLoc2 = (Product)productPra2;
//		return productLoc.getName().compareTo(productLoc2.getName());
//	}
}


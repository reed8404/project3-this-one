package utilities;

/*awt*/
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/*io*/
import java.io.Console;
import java.io.File;
import java.io.IOException;

/*util*/
import java.util.Iterator;
import java.util.List;

/*imageio*/
import javax.imageio.ImageIO;

/*jansi*/
import org.fusesource.jansi.*;

/*app*/
import app.*;

public class Displayer {
	/*
	 * Ansi Escape Sequences, provide coloring, styling, etc..., 
	 * viewable in windows console via Jansi
	 */
	public static final String BLACK = "\u001B[0;30m";
    public static final String RED = "\u001B[0;31m";
    public static final String GREEN = "\u001B[0;32m";
    public static final String YELLOW = "\u001B[0;33m";
    public static final String BLUE = "\u001B[0;34m";
    public static final String MAGENTA = "\u001B[0;35m";
    public static final String CYAN = "\u001B[0;36m";
    public static final String WHITE = "\u001B[0;37m";
    public static final String ANSI_CLS = "\u001b[2J";
    public static final String ANSI_HOME = "\u001b[H";
    public static final String ANSI_BOLD = "\u001b[1m";
    public static final String ANSI_AT55 = "\u001b[10;10H";
    public static final String ANSI_REVERSEON = "\u001b[7m";
    public static final String ANSI_NORMAL = "\u001b[0m";
    public static final String ANSI_WHITEONBLUE = "\u001b[37;44m";
    
    /**
     * 
     * @param productList(List<Product>) a list of products/inventory
     */
    public static void displayProduct(List<Product> productList) {
		 AnsiConsole.systemInstall(); 
//		 Console console = null;
		 Iterator<Product> iterator = WareHouse.productList.iterator();
		 try {
//			 console = System.console();
			 
//			 if(console != null) {
				 String format = "%1$4s %2$10s %3$10s%n";
			 	while (iterator.hasNext()) {
			 		Product product = (Product)iterator.next();
			 		
			 		makeBlue(String.format(format, "Name", "Category", "Price"));
			 		makeMagenta(String.format(format, "-----", "-----", "-----"));
			        makeGreen(String.format(format, product.getName(), product.getCategory(), product.getPrice()));
//			        TO-DO: Alternate printing of products between green & yellow(every-other)
//			        makeYellow(String.format(format, product.getName(), product.getCategory(), product.getPrice()));
			 	}
//			 } 	
		 }
		 catch(Exception exception) {
			exception.printStackTrace(); 
		 }
		 AnsiConsole.systemUninstall();
	} //end of displayProduct
    
/*********************Ansi color alteration********************/
	
    /**
     * turn String blue
     * @param blueString(String), String value to be turned blue
     */
	public static void makeBlue(String blueString) {
		AnsiConsole.out.println(BLUE + blueString + ANSI_NORMAL);
	}
	
	/**
	 * turns String green
	 * @param greenString(String), String value to be turned green
	 */
	public static void makeGreen(String greenString) {
		AnsiConsole.out.println(GREEN + greenString + ANSI_NORMAL);
	}
	
	/**
	 * turns String yellow
	 * @param yellowString(String), String value to be turned yellow
	 */
	public static void makeYellow(String yellowString) {
		AnsiConsole.out.println(YELLOW + yellowString + ANSI_NORMAL);
	}
	
	/**
	 * turns String megenta
	 * @param magentaString(String), String value to be turned megenta
	 */
	public static void makeMagenta(String magentaString) {
		AnsiConsole.out.println(MAGENTA + magentaString + ANSI_NORMAL);
	}
	
    /**
     * displays Survival Store 'logo'
     * 
     * declare-instantiate-initialize, bufferedImage, pass in: 
     * width(int) - width of the created image
     * height(int) - height of the created image
     * imageType(int) - type of the created image
     * TYPE_INT_RGB(int), image with 8-bit RGB color components packed into integer pixels
     * 
     * declare-instantiate, graphics(Graphics), to encapsulate state information returned from createGraphics
     * needed for the basic rendering operations
     * 
     * createGraphics, creates a Graphics2D, which can be used to draw into this BufferedImage
     * 
     * set graphics context's font to the specified font:
     * instantiate-initialize, Font, creates a new Font from the specified: 
     * name - Dialog, a family name for a logical font
     * style - PLAIN, a constant for the Font(renders text in a visible way) The style argument is an integer bitmask 
     * point size - the point size of the Font
     * 
     * declare-initialize, graphics2D, cast graphics to Graphics2D
     * set the value of a single preference for the rendering algorithms:
     * hintKey - the key(KEY_TEXT_ANTIALIASING) of the hint to be set
     * hintValue - the value(VALUE_TEXT_ANTIALIAS_ON) indicating preferences for the specified hint category
     * drawString renders the text of the specified String(SurvivalStore), 
     * using the current text attribute state in the Graphics2D context:
     * str - the string to be rendered
     * x - the x coordinate of the location where the String should be rendered
     * y - the y coordinate of the location where the String should be rendered
     * 
     * try, write an image using an arbitrary ImageWriter(bufferedImage) 
     * that supports the given format(png) to a File(text.png)
     * 
     * catch e(IOException), if failed or interrupted I/O operation, 
     * prints e and its backtrace to the standard error stream
     * 
     * loop while (y(int) EQUALS 0 is LESS THAN 32) increment y by 1 each iteration,
     * every iteration, declare-instantiate-initialize, StringBuilder
     * 
     * loop while (x(int) EQUALS 0 is LESS THAN 144) increment x by 1 each iteration,
     * check if bufferedImage's integer pixel in the default RGB color model and default sRGB colorspace,
     * are EQUIVALENT to -16777216, if ture, append space, if false, append "#", if bufferedImage integer pixel 
     * in the default RGB color model and default sRGB colorspace, are EQUIVALENT to -1, & "*" if not EQUIVALENT to -1
     * 
     * if (stringBuilder is empty) continue
     * 
     * print " " or "#" or "*"
     * 
     * @BufferedImage describes an Image with an accessible buffer of image data
     * 
     * @Graphics abstract base class for all graphics contexts that allow an application to
     * draw onto components that are realized on various devices, as well as onto off-screen images
     * 
     * @Graphics2D extends the Graphics class to provide more sophisticated control over geometry, 
     * coordinate transformations, color management, and text layout
     * 
     * @ImageIO class containing static convenience methods for locating 
     * ImageReaders and ImageWriters, and performing simple encoding and decoding
     * 
     * @StringBuilder A mutable sequence of characters
     * 
     */
    public static void displaySurvivalStoreLogo() {
    	BufferedImage bufferedImage = new BufferedImage(144, 32, BufferedImage.TYPE_INT_RGB);
    	
		Graphics graphics = bufferedImage.createGraphics();
		graphics.setFont(new Font("Dialog", Font.PLAIN, 24));
		
		Graphics2D graphics2d = (Graphics2D) graphics;
		graphics2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		graphics2d.drawString("SurvivalStore", 6, 24);
		
		try {
			ImageIO.write(bufferedImage, "png", new File("text.png"));
		} 
		catch (IOException e) {
			e.printStackTrace();
		}

		for (int y = 0; y < 32; y++) {
		    StringBuilder stringBuilder = new StringBuilder();
		    
		    for (int x = 0; x < 144; x++) {
		        stringBuilder.append(bufferedImage.getRGB(x, y) == -16777216 ? " " : bufferedImage.getRGB(x, y) == -1 ? "#" : "*");
		    }
		    
		    if (stringBuilder.toString().trim().isEmpty()) {
		    	continue;
		    }
		    
		    System.out.println(stringBuilder);
		}
    } //end of displaySurvivalStore
}
